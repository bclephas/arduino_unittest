/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include "Arduino.h"
#include <cstddef>

#include "CppUTestExt/MockSupport.h"

static ArduinoMock* arduinoMock = nullptr;
ArduinoMock* arduinoMockInstance() {
    if(!arduinoMock) {
        arduinoMock = new ArduinoMock();
    }
    return arduinoMock;
}
void releaseArduinoMock() {
    if(arduinoMock) {
        delete arduinoMock;
        arduinoMock = nullptr;
    }
}

/* Arduino interface */
void pinMode(uint8_t pin, uint8_t mode) {
    arduinoMock->pinMode(pin, mode);
}
void analogWrite(uint8_t pin, int value) {
    arduinoMock->analogWrite(pin, value);
}
void digitalWrite(uint8_t pin, uint8_t value) {
    arduinoMock->digitalWrite(pin, value);
}
int analogRead(uint8_t pin) {
    return arduinoMock->analogRead(pin);
}
int digitalRead(uint8_t pin) {
	return arduinoMock->digitalRead(pin);
}
void delay(unsigned long pin) {
    arduinoMock->delay(pin);
}
unsigned long millis(void) {
    return arduinoMock->millis();
}

void analogReference(uint8_t mode) {
}

unsigned long micros(void) {
    return 0;
}
void delayMicroseconds(unsigned int us) {
    delay(us / 1000);
}
unsigned long pulseIn(uint8_t pin, uint8_t state, unsigned long timeout) {
    return 0;
}

void shiftOut(uint8_t dataPin, uint8_t clockPin, uint8_t bitOrder, uint8_t val) {
}
uint8_t shiftIn(uint8_t dataPin, uint8_t clockPin, uint8_t bitOrder) {
    return 0;
}

void attachInterrupt(uint8_t interruptNum, void (*userFunc)(void), int mode) {
    arduinoMock->attachInterrupt(interruptNum, userFunc, mode);
}
void detachInterrupt(uint8_t interruptNum) {
    arduinoMock->detachInterrupt(interruptNum);
}


/* Arduino Mock */
void ArduinoMock::pinMode(uint8_t pin, uint8_t mode) {
	mock().actualCall("pinMode").withParameter("pin", pin).withParameter("mode", mode);
}
void ArduinoMock::analogWrite(uint8_t pin, int value) {
	mock().actualCall("analogWrite").withParameter("pin", pin).withParameter("value", value);
}
int ArduinoMock::digitalRead(uint8_t pin) {
    return mock().actualCall("digitalRead").withParameter("pin", pin).returnIntValueOrDefault(0);
}
void ArduinoMock::digitalWrite(uint8_t pin, uint8_t value) {
	mock().actualCall("digitalWrite").withParameter("pin", pin).withParameter("value", value);
}
int ArduinoMock::analogRead(int pin) {
	return mock().actualCall("analogRead").withParameter("pin", pin).returnIntValueOrDefault(0);
}
void ArduinoMock::delay(int ms) {
	mock().actualCall("delay").withParameter("ms", ms);
}
unsigned long ArduinoMock::millis() {
	return mock().actualCall("millis").returnUnsignedLongIntValueOrDefault(0);
}
void ArduinoMock::attachInterrupt(uint8_t interruptNum, void (*userFunc)(void), int mode) {
    mock().actualCall("attachInterrupt")
        .withParameter("interruptNum", interruptNum)
        //.withParameter("userFunc", userFunc)
        .withParameter("mode", mode);

    callback = std::make_tuple(interruptNum, userFunc);
}
void ArduinoMock::detachInterrupt(uint8_t interruptNum) {
    mock().actualCall("detachInterrupt").withParameter("interruptNum", interruptNum);

    callback = std::make_tuple(255, nullptr);
}

void ArduinoMock::fireInterrupt(uint8_t interruptNum) {
    uint8_t interrupt = std::get<0>(callback);
    void (*cb)(void)  = std::get<1>(callback);

    if (interrupt == interruptNum) {
        cb();
    }
}


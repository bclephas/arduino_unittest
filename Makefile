ifndef ARDUINO_HOME
  $(error ARDUINO_HOME not set)
endif

ifndef CPPUTEST_HOME
  $(error CPPUTEST_HOME not set)
endif

CPPFLAGS      := -I$(CPPUTEST_HOME)/include
CXXFLAGS      := -ggdb -include $(CPPUTEST_HOME)/include/CppUTest/MemoryLeakDetectorNewMacros.h -std=c++11
CFLAGS        := -ggdb -include $(CPPUTEST_HOME)/include/CppUTest/MemoryLeakDetectorMallocMacros.h
LD_LIBRARIES  := -L$(CPPUTEST_HOME)/cpputest_build/lib -L.
ARDUINO_LIBS  := -I$(ARDUINO_HOME)/hardware/arduino/avr/cores/arduino/ \
                 -I$(ARDUINO_HOME)/hardware/tools/avr/lib/avr/include/ \
                 -I$(ARDUINO_HOME)/hardware/tools/avr/avr/include/ \
                 -I$(ARDUINO_HOME)/hardware/arduino/avr/variants/standard/

# Disable built-in rules
.SUFFIXES:

all: test_blink test_statechanged test_attachinterrupt

# Uses the real Arduino.h
%.arduino.o: %.ino
	cpp $< > $<.cpp
	sed -i '1i #include <SerialMock.h>' $<.cpp
	sed -i '1i #include <Arduino.h>' $<.cpp
	g++ -ggdb -std=c++11 $(ARDUINO_LIBS) -I. $<.cpp -c -o $@

# Uses the fake Arduino.h
libarduino_stub.so: Arduino.cpp Arduino.h SerialMock.cpp
	g++ -fPIC -shared $(CPPFLAGS) $(CXXFLAGS) -I. -o libarduino_stub.so Arduino.cpp SerialMock.cpp

# Uses the fake Arduino.h
test_blink: blink.arduino.o libarduino_stub.so test_blink.cpp Arduino.h
	g++ $(CPPFLAGS) $(CXXFLAGS) $(LD_LIBRARIES) -o test_blink test_blink.cpp -Wl,-rpath=. -Wl,-rpath=$(CPPUTEST_HOME)/cpputest_build/lib blink.arduino.o -lCppUTest -lCppUTestExt -larduino_stub

# Uses the fake Arduino.h
test_statechanged: StateChangeDetection.arduino.o libarduino_stub.so test_statechanged.cpp Arduino.h
	g++ $(CPPFLAGS) $(CXXFLAGS) $(LD_LIBRARIES) -o test_statechanged test_statechanged.cpp -Wl,-rpath=. -Wl,-rpath=$(CPPUTEST_HOME)/cpputest_build/lib StateChangeDetection.arduino.o -lCppUTest -lCppUTestExt -larduino_stub

# Uses the fake Arduino.h
test_attachinterrupt: attachinterrupt.arduino.o libarduino_stub.so test_attachinterrupt.cpp Arduino.h
	g++ $(CPPFLAGS) $(CXXFLAGS) $(LD_LIBRARIES) -o test_attachinterrupt test_attachinterrupt.cpp -Wl,-rpath=. -Wl,-rpath=$(CPPUTEST_HOME)/cpputest_build/lib attachinterrupt.arduino.o -lCppUTest -lCppUTestExt -larduino_stub

run: all
	find -executable -name test_\* -exec {} \;

.PHONY: all clean
clean:
	rm -f libarduino_stub.so
	rm -f *.arduino.o
	rm -f *.ino.cpp
	rm -f test_blink
	rm -f test_statechanged
	rm -f test_attachinterrupt


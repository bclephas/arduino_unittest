# Arduino unit test

Demonstration of how to build a simple unit test (CppUTest) to test an Arduino
sketch off-chip. The example is quick to be modified to Google Test / Mock.

The used sketch is the Blink example sketch shipped with Arduino.

Adapted the Arduino Mock from https://github.com/balp and its fork
https://github.com/ikeyasu/arduino-mock

This is in no means meant to be complete. It serves only as a reference, a
guide, an example, a proof-of-concept.

## How to build

Set ARDUINO_HOME and CPPUTEST_HOME according to your environment and run
make. Ensure that CppUTest is already built.


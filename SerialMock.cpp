// Declares SerialMock as Serial
#include "SerialMock.h"
#include "Arduino.h"

#include "CppUTestExt/MockSupport.h"

#include <iostream>

void SerialMock::begin(unsigned long baud)
{
    begin(baud, SERIAL_8N1);
}

void SerialMock::begin(unsigned long baud, byte config)
{
    mock().actualCall("begin")
        .withParameter("baud", baud)
        .withParameter("config", config);
}

void SerialMock::end()
{
    mock().actualCall("end");
}

uint8_t SerialMock::available(void)
{
    return mock().actualCall("available").returnIntValueOrDefault(0);
}

uint8_t SerialMock::read(void)
{
    return mock().actualCall("read").returnIntValueOrDefault(0);
}

size_t SerialMock::write(uint8_t c)
{
    return mock().actualCall("write")
        .withParameter("c", c)
        .returnIntValueOrDefault(0U);
}

size_t SerialMock::print(int s, int base)
{
    if (loggingEnabled) std::cout << s;
    return mock().actualCall("print")
        .withParameter("s", s)
        .withParameter("base", base)
        .returnIntValueOrDefault(0U);
}

size_t SerialMock::print(const char* s)
{
    if (loggingEnabled) std::cout << s;
    return mock().actualCall("print")
        .withParameter("s", s)
        .returnIntValueOrDefault(0U);
}

size_t SerialMock::println(int s, int base)
{
    if (loggingEnabled) std::cout << s << std::endl;
    return mock().actualCall("println")
        .withParameter("s", s)
        .withParameter("base", base)
        .returnIntValueOrDefault(0U);
}

size_t SerialMock::println(const char* s)
{
    if (loggingEnabled) std::cout << s << std::endl;
    return mock().actualCall("println")
        .withParameter("s", s)
        .returnIntValueOrDefault(0U);
}

SerialMock::SerialMock()
    : loggingEnabled(false) {
}

void SerialMock::enableLogging() {
    loggingEnabled = true;
}

void SerialMock::disableLogging() {
    loggingEnabled = false;
}

SerialMock Serial;


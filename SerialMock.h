#ifndef SERIAL_MOCK_H
#define SERIAL_MOCK_H

#include <cstdint>
#include <cstddef>

#define byte uint8_t

#define SERIAL_8N1 0x06

#define DEC 10
#define HEX 16
#define OCT 8
#define BIN 2

class SerialMock
{
public:
    uint8_t available(void);
    uint8_t read(void);

    void begin(unsigned long baud);
    void begin(unsigned long baud, byte config);
    void end();

    size_t write(uint8_t c);

    size_t print(int s, int base = DEC);
    size_t print(const char* s);
    size_t println(int s, int base = DEC);
    size_t println(const char* s);

public:
    SerialMock();
    void enableLogging();
    void disableLogging();

private:
    bool loggingEnabled;
};

extern SerialMock Serial;

#endif // SERIAL_MOCK_H


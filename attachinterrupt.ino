// Example taken from https://www.arduino.cc/en/Reference/AttachInterrupt?action=sourceblock&num=1

const byte ledPin = 13;
const byte interruptPin = 2;
volatile byte state = LOW;

void blink();

void setup() {
    // Ensure state is LOW
    state = LOW;

    pinMode(ledPin, OUTPUT);
    pinMode(interruptPin, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(interruptPin), blink, CHANGE);
}

void loop() {
    digitalWrite(ledPin, state);
}

void blink() {
    state = !state;
}

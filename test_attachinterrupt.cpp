/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include "Arduino.h"
#include "CppUTest/CommandLineTestRunner.h"
#include "CppUTest/TestHarness.h"
#include "CppUTestExt/MockSupport.h"

TEST_GROUP(TestAttachInterrupt)
{
    ArduinoMock *arduinoMock;

    void setup() {
        arduinoMock = arduinoMockInstance();

        mock().expectOneCall("pinMode").withParameter("pin", 13).withParameter("mode", OUTPUT);
        mock().expectOneCall("pinMode").withParameter("pin", 2).withParameter("mode", INPUT_PULLUP);
        mock().expectOneCall("attachInterrupt")
            .withParameter("interruptNum", 0)
            //.withParameter("userFunc", ...)
            .withParameter("mode", CHANGE);

        // interrupt = digitalPinToInterrupt(pin)
        ::setup();
    }

    void teardown() {
        mock().checkExpectations();
        mock().clear();
        releaseArduinoMock();
        arduinoMock = nullptr;
    }
};

TEST(TestAttachInterrupt, VerifyStateIsLowWhenNoInterruptsHappened)
{
    mock().expectOneCall("digitalWrite").withParameter("pin", 13).withParameter("value", LOW);

    loop();
}

TEST(TestAttachInterrupt, VerifyStateIsHighWhenOneInterruptHappened)
{
    mock().expectOneCall("digitalWrite").withParameter("pin", 13).withParameter("value", HIGH);
    arduinoMock->fireInterrupt(0);

    loop();
}

TEST(TestAttachInterrupt, VerifyStateIsLowWhenTwoInterruptHappened)
{
    mock().expectOneCall("digitalWrite").withParameter("pin", 13).withParameter("value", LOW);
    arduinoMock->fireInterrupt(0);
    arduinoMock->fireInterrupt(0);

    loop();
}

int main(int ac, char** av)
{
    return CommandLineTestRunner::RunAllTests(ac, av);
}


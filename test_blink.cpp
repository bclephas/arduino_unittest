/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include "Arduino.h"
#include "CppUTest/CommandLineTestRunner.h"
#include "CppUTest/TestHarness.h"
#include "CppUTestExt/MockSupport.h"

TEST_GROUP(FirstTestGroup)
{
    void setup() {
		arduinoMockInstance();
		//mock().crashOnFailure();
	}
	
    void teardown() {
		mock().checkExpectations();
		releaseArduinoMock();
		mock().clear();
	}
};

TEST(FirstTestGroup, ValidateArduinoConstants)
{
	CHECK_EQUAL(1, OUTPUT);
	CHECK_EQUAL(1, HIGH);
	CHECK_EQUAL(0, LOW);
}

TEST(FirstTestGroup, EnsureSetupConfiguresCorrectly)
{
	mock().expectOneCall("pinMode").withParameter("pin", 13).withParameter("mode", OUTPUT);

	::setup(); // -> calls setup of the arduino code and not the setup of the test group
}

TEST(FirstTestGroup, LoopOnceWillToggleHighAndLow)
{
	mock().expectOneCall("pinMode").withParameter("pin", 13).withParameter("mode", OUTPUT);

	mock().expectOneCall("digitalWrite").withParameter("pin", 13).withParameter("value", HIGH);
	mock().expectOneCall("digitalWrite").withParameter("pin", 13).withParameter("value", LOW);
	mock().expectNCalls(2, "delay").withParameter("ms", 1000);

	::setup();
	loop();
}

int main(int ac, char** av)
{
    return CommandLineTestRunner::RunAllTests(ac, av);
}


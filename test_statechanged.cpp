/* vim: set filetype=cpp tabstop=4 shiftwidth=4 noautoindent smartindent: */

#include "Arduino.h"
#include "CppUTest/CommandLineTestRunner.h"
#include "CppUTest/TestHarness.h"
#include "CppUTestExt/MockSupport.h"

TEST_GROUP(StateChanged)
{
    void setup() {
		arduinoMockInstance();

	    mock().expectOneCall("pinMode").withParameter("pin", 2).withParameter("mode", INPUT);
	    mock().expectOneCall("pinMode").withParameter("pin", 13).withParameter("mode", OUTPUT);
        mock().expectOneCall("begin").withParameter("baud", 9600).withParameter("config", 0x06); /* 0x06 = 8N1 */
    }

    void teardown() {
		mock().checkExpectations();
		releaseArduinoMock();
		mock().clear();
	}
};

TEST(StateChanged, ButtonPressedOnceLeavesLedDisabled)
{
    mock().expectOneCall("digitalRead").withParameter("pin", 2).andReturnValue(HIGH);
    mock().expectNCalls(2, "println").ignoreOtherParameters();
    mock().expectOneCall("print").ignoreOtherParameters();
    mock().expectOneCall("delay").withParameter("ms", 50);
    mock().expectOneCall("digitalWrite").withParameter("pin", 13).withParameter("value", LOW);

    ::setup();
    loop();
}

TEST(StateChanged, ButtonPressedOnceInMultipleLoopsLeavesLedDisabled)
{
    mock().expectNCalls(3, "digitalRead").withParameter("pin", 2).andReturnValue(HIGH);
    mock().expectNCalls(2, "println").ignoreOtherParameters();
    mock().expectOneCall("print").ignoreOtherParameters();
    mock().expectOneCall("delay").withParameter("ms", 50);
    mock().expectNCalls(3, "digitalWrite").withParameter("pin", 13).withParameter("value", LOW);

    ::setup();
    for (int i = 0; i < 3; i++) {
        loop();
    }
}

// Freaking ugly!!
TEST(StateChanged, ButtonPressed4TimesEnablesLed)
{
    ::setup();

    int buttonState = LOW;
    for (int i = 0; i < 7; i++) {
        mock().clear();

        buttonState = 1 - buttonState;
        mock().expectOneCall("digitalRead").withParameter("pin", 2).andReturnValue(buttonState);

        if (buttonState == HIGH) {
            mock().expectNCalls(2, "println").ignoreOtherParameters();
            mock().expectOneCall("print").ignoreOtherParameters();
        } else if (buttonState == LOW) {
            mock().expectOneCall("println").ignoreOtherParameters();
        }
        mock().expectOneCall("delay").withParameter("ms", 50);

        if (i < 6)
            mock().expectOneCall("digitalWrite").withParameter("pin", 13).withParameter("value", LOW);
        else
            mock().expectOneCall("digitalWrite").withParameter("pin", 13).withParameter("value", HIGH);

        loop();
    }
}
IGNORE_TEST(StateChanged, ButtonPressed4TimesEnablesLed)
{
    // Need to find a way to send HIGH, LOW, HIGH, LOW, HIGH, LOW, HIGH (7 loops)
    mock().expectNCalls(7, "digitalRead").withParameter("pin", 2)
        .andReturnValue(HIGH)
        .andReturnValue(LOW)
        .andReturnValue(HIGH)
        .andReturnValue(LOW)
        .andReturnValue(HIGH)
        .andReturnValue(LOW)
        .andReturnValue(HIGH);
    mock().expectNCalls(2, "println").ignoreOtherParameters();
    mock().expectOneCall("print").ignoreOtherParameters();
    mock().expectNCalls(6, "delay").withParameter("ms", 50);
    mock().expectNCalls(7, "digitalWrite").withParameter("pin", 13).ignoreOtherParameters();

    ::setup();
    for (int i = 0; i < 7; i++) {
        loop();
    }
}

int main(int ac, char** av)
{
    return CommandLineTestRunner::RunAllTests(ac, av);
}

